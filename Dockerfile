FROM codercom/code-server:4.2.0

USER root
# Added glibc-source so examinees are not presented with `unable to find file` when stepping into libraries
RUN apt-get update -y \
        && apt-get upgrade -y \
        && apt-get install -y python3-dev python3-pip gcc gdb git glibc-source python3-venv zlib1g-dev curl\
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*
# Extract glibc-source files from/to /usr/src/glibc/ so they're available to the debugger
#NOTE: be sure to check if version number needs to be udpated when updating code-server container
RUN tar xvf /usr/src/glibc/glibc-2.31.tar.xz -C /usr/src/glibc/

ARG USERNAME=coder
ARG HOME=/home/$USERNAME
ARG CODE_BASE_SHARE_DIR=${HOME}/.local/share/code-server
ARG CODE_KEYBIND_DIR=${CODE_BASE_SHARE_DIR}/User
ARG CODE_EXTENTIONS_DIR=${CODE_BASE_SHARE_DIR}/extensions
ARG CONFIG_DIR=${HOME}/.config/code-server

USER $USERNAME
WORKDIR $HOME

RUN code-server --install-extension ms-python.python \
    && code-server --install-extension ms-vscode.cpptools \
    && code-server --install-extension bierner.markdown-mermaid

# for some reason we need to install the specific extention for the debuger to function correctly.
# Copy VS Code extentions for cpptools 
COPY ms-vscode.cpptools-1.9.7@linux-x64.vsix .
RUN code-server --install-extension ms-vscode.cpptools-1.9.7@linux-x64.vsix

# Disabled download, uncompress & install due to the potential for the links to be blocked because of exceeding Microsoft's threshold
# We are keeping the code in case we want to go back to this in the future
# Install specific packages
#RUN curl https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-vscode/vsextensions/cpptools/1.1.3/vspackage -o ms-tools.cpptools.gz && \
#    gunzip -c ms-tools.cpptools.gz > ms-tools.cpptools.vsix && \
    # use `|| true` b/c there's an issue with code-server sending back 500 server errors even though the extension installs correctly
#    code-server --install-extension ms-tools.cpptools.vsix || true
#RUN curl https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-python/vsextensions/python/2020.10.332292344/vspackage -o ms-tools.python.gz && \
#    gunzip -c ms-tools.python.gz > ms-tools.python.vsix && \
#    code-server --install-extension ms-tools.python.vsix || true

# INSTALL SOME DEPENDENCIES AND PUT THEM ON PATH (SUCH A cmake) 
COPY requirements.txt .
RUN if [ -f "requirements.txt" ]; then pip3 install --user -r requirements.txt; fi
ENV PATH="/home/coder/.local/bin:$PATH"
COPY deploy-settings.json $CODE_KEYBIND_DIR/settings.json

# Add files that will be volume mounted so they don't become folders due to not existing
# This is the config file for VS Code Server
RUN if [ ! -d "${CONFIG_DIR}}" ]; then mkdir -p ${CONFIG_DIR}; fi \
    # &&  if [ ! -f "${CONFIG_DIR}/config.yaml" ]; then touch "${CONFIG_DIR}/config.yaml"; fi \
    # This is the keybindings settings for VS Code Server
    && if [ ! -d "${CODE_KEYBIND_DIR}" ]; then mkdir -p ${CODE_KEYBIND_DIR}; fi \
    && if [ ! -f "${CODE_KEYBIND_DIR}/keybindings.json" ]; then touch "${CODE_KEYBIND_DIR}/keybindings.json"; fi \
    # This is the extensions dir searched is VS Code Server
    && if [ ! -d "${CODE_EXTENTIONS_DIR}" ]; then mkdir -p ${CODE_EXTENTIONS_DIR}; fi


# Ensure coder has access to the directories and files within its home directory
RUN sudo chown -Rh ${USER}:${USER} ${HOME}

ENTRYPOINT ["dumb-init", "fixuid", "-q", "/usr/bin/code-server", "/home/coder/exam", "--user-data-dir=.local/share/code-server"]
