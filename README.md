# Purpose

The VS Code Server environment uses docker-compose to create a separate VS Code Server environment for each individual 
taking an exam as well as a separate networking server. In future iterations, this environment will support a study 
environment.

# Documentation
- [VS Code Server Setup](docs/guides/vs_code_server_setup.md)
- [VS Code Server Maintenance](docs/guides/vs_code_server_maintenance.md)
- [VS Code Server Usage](docs/guides/vs_code_server_usage.md)
