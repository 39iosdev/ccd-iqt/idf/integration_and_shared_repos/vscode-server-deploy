#!/bin/bash
if [[ $EUID -ne 0 ]]
then
   echo "This script must be run as root"
   exit 1
fi

getHelp()
{
    echo "Syntax: $0 <options>"
    echo
    echo "This will enable a persistent swap on your system."
    echo
    echo "  <options>"
    echo
    echo "    -f <path>    Specify the path to the system swap file; the default is /swapfile"
    echo "    -b <size>    Specify the block size used in swap; the default is 128M"
    echo "                 Valid disk size units are: c, w, b, K, KB, M, MB, G, GB, T, TB, P, PB, E, EB, Z, ZB, Y, YB"
    echo "                 c = 1, w = 2, b = 512, K = 1024, KB = 1000, M = 1024*1024, MB = 1000*1000, etc."
    echo "    -n <number>  Specify the number of blocks in swap; the default is 32"
    echo "    -h, --help   Display this help menu"
    exit
}

# This strips nondigit characters from a string and returns the result
getDigits()
{
    if [ $(echo $1 | sed 's/[^0-9]//g') == "" ]
    then
        return 0
    else
        return $(echo $1 | sed 's/[^0-9]//g')
    fi
}

# This gets the necessary conversion value for a specified unit
getConversion()
{
    case "$1" in
    c)
        CONVERSION=1
        ;;
    w)
        CONVERSION=2
        ;;
    b)
        CONVERSION=512
        ;;
    K)
        CONVERSION=1024
        ;;
    KB)
        CONVERSION=1000
        ;;
    M)
        CONVERSION=$((1024*1024))
        ;;
    MB)
        CONVERSION=$((1000*1000))
        ;;
    G)
        CONVERSION=$((1024*1024*1024))
        ;;
    GB)
        CONVERSION=$((1000*1000*1000))
        ;;
    T)
        CONVERSION=$((1024*1024*1024*1024))
        ;;
    TB)
        CONVERSION=$((1000*1000*1000*1000))
        ;;
    P)
        CONVERSION=$((1024*1024*1024*1024*1024))
        ;;
    PB)
        CONVERSION=$((1000*1000*1000*1000*1000))
        ;;
    E)
        CONVERSION=$((1024*1024*1024*1024*1024*1024))
        ;;
    EB)
        CONVERSION=$((1000*1000*1000*1000*1000*1000))
        ;;
    Z)
        CONVERSION=$(bc <<< 1024*1024*1024*1024*1024*1024*1024)
        ;;
    ZB)
        CONVERSION=$(bc <<< 1000*1000*1000*1000*1000*1000*1000)
        ;;
    Y)
        CONVERSION=$(bc <<< 1024*1024*1024*1024*1024*1024*1024*1024)
        ;;
    YB)
        CONVERSION=$(bc <<< 1000*1000*1000*1000*1000*1000*1000*1000)
        ;;
    *)
        CONVERSION=0
        ;;
esac
}

# This determines if there is sufficent space for the requested swap allocation
# first param:  the absolute path to the desired swap file
# second param: the desired size of swap as calculated by input block size and count
# third param:  a number needed to convert bytes to the specified unit in the block size
checkDiskUsage()
{
    # Determin if the parent directory exists
    if [[ -d $(dirname $1) ]]
    then
        # Determin if an absolute path was entered
        if [[ $(echo $1 | grep -e ^/) != "" ]]
        then
            INDEX=0
            DSKSZTOTAL=0
            DSKSZAVAIL=0
            for VALUE in $(df -B1 $(dirname $1) | grep -v Available)
            do
                # Get the total volume size
                if [[ $INDEX -eq 1 ]]
                then
                    DSKSZTOTAL=$VALUE
                fi
                # Get the space remaining
                if [[ $INDEX -eq 3 ]]
                then
                    DSKSZAVAIL=$VALUE
                fi
                INDEX=$(($INDEX + 1))
            done
            # Determine if there is ~15% space remaining after swap is allocated
            # First, convert the total and available space in bytes to the specified unit format; as is indicated in $3
            DSKSZTOTALBYUNIT=$(bc <<< "$DSKSZTOTAL / $3")
            DSKSZAVAILBYUNIT=$(bc <<< "$DSKSZAVAIL / $3")
            # Make sure the requested size format is not too large for our calculation
            if [[ $DSKSZTOTALBYUNIT -lt 1 || $DSKSZAVAILBYUNIT -lt 1 ]]
            then
                echo "The specified disk units are too large"
                return 0
            fi
            # Next, determine if the remaining space, after the swap allocation, is greater than ~15% of total space
            # $2 represents the spaced used by swap and division by 1 gits rid of the decimal part for easy comparison
            if [[ $(bc <<< "$DSKSZAVAILBYUNIT - $2") -gt $(bc <<< "$DSKSZTOTALBYUNIT * .15 / 1") ]]
            then
                return 1 # There's at least 15% disk space available
            else
                echo "The generated swap file would use too much space. Free some space or use a smaller swap."
                return 0
            fi
        else
            echo "Please use absolute paths"
            return 0
        fi
    else
        echo "The path wasn't valid"
        return 0
    fi
    echo "Unable to determine the available disk space. Please ensure your system has sufficient space for the swap."
    return 0
}

validateUnits()
{
    # Determine if we have valid units
    case "$1" in
        c|w|b|K|KB|M|MB|G|GB|T|TB|P|PB|E|EB|Z|ZB|Y|YB)
            return 1 # Units are valid
            ;;
        *)
            return 0 # Units are invalid
            ;;
    esac
}

# Determine if the help menu was requested
for option in "$@"
do
    if [[ "$option" == "-h" || "$option" == "--help" ]]
    then
        getHelp
    fi
done

# Setup the inital values
SWAPPATH=/swapfile
BLKSZ=128M
NUMBLKS=32

# Get user defined values
while getopts f:b:n: flag
do
    case "${flag}" in
	f) SWAPPATH="${OPTARG}";;
	b) BLKSZ=${OPTARG};;
	n) NUMBLKS=${OPTARG};;
    esac
done

getDigits "$BLKSZ"
TOTALSWAP=$(($? * $NUMBLKS))
UNITS=$(echo "$BLKSZ" | sed 's/[^A-Za-z]//g')

if [[ $UNITS == "" ]]
then
    UNITS="c"  # In case the parsed value comes back an empty string
fi

validateUnits $UNITS
if [[ $? -eq 0 ]]
then
    getHelp
    echo "The specified disk size units was invalid."
    exit
fi

getConversion $UNITS

if [[ $CONVERSION -eq 0 || $CONVERSION == "" ]]
then
    echo "Unable to determine disk size conversion ratio"
    exit
fi

checkDiskUsage $SWAPPATH $TOTALSWAP $CONVERSION
if [[ $? -eq 0 ]]
then
    exit
fi

echo
echo "This will enable swap on your system so it is persistent."
echo
echo "The following will be set:"
echo
echo "    Path for swap:      $SWAPPATH"
echo "    Block size:         $BLKSZ"
echo "    Number of Blocks:   $NUMBLKS"
echo "    Total space needed: $TOTALSWAP$UNITS"
echo
read -p "Is this Okay? " createSwap

case "$createSwap" in
    Y|y|Yes|yEs|yeS|YEs|YeS|yES|YES|yes)
        if [[ "$(swapon -s)" == "" ]]
        then
            echo "Enabling swap"
            dd if=/dev/zero of="$SWAPPATH" bs=$BLKSZ count=$NUMBLKS
            chmod 600 "$SWAPPATH"
            mkswap "$SWAPPATH" 
            swapon "$SWAPPATH"
            swapon -s
            echo "$SWAPPATH swap swap defaults 0 0" >> /etc/fstab
            echo "$SWAPPATH created and persistent through reboot"
        else
            echo "Swap is already enabled on this system"
        fi
	;;
    N|n|No|nO|NO|no)
        echo "Bye"
	;;
    *)
        echo "Please answer yes or no"
        ;;
esac

